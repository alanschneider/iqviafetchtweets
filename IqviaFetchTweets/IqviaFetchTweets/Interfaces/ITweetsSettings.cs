﻿namespace IqviaFetchTweets.Interfaces
{
    public interface ITweetsSettings
    {
        int MaxYearsIntoPast { get; }
    }
}
